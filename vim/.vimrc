
"Vim plugins installed by plug
call plug#begin('~/.vim/plugged')
Plug 'vimwiki/vimwiki'
Plug 'preservim/nerdtree'
Plug 'jiangmiao/auto-pairs'

call plug#end()


"Some basic settings for syntax highlighting 
"as well as tab spacing and line numbering

syntax on
set tabstop=4
set number



"Custom key mappings

"Toggle nerd tree
map <C-n> :NERDTreeToggle<CR>
" maps Ctrl + v to open a vertical split
map <C-v> :vsplit newfile<CR>


"Quality of life improvements to make vim sexy


"enable auto completion
set wildmode=longest,list,full

" automated install of plug if not installed already
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif


"automatically launch nerd tree vim plugin
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

